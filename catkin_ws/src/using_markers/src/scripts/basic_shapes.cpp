#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#include <cmath>
#include <math.h>

int main( int argc, char** argv )
{
  ros::init(argc, argv, "basic_shapes");
  ros::NodeHandle n;
  ros::Rate r(1);
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);

  // Set our initial shape type to be a cube
  uint32_t shape = visualization_msgs::Marker::CUBE;

  while (ros::ok())
  {
    visualization_msgs::Marker shuriken, kolumna, okrag, linia1, linia2, linia3, linia4;
    // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    shuriken.header.frame_id = kolumna.header.frame_id = okrag.header.frame_id = linia1.header.frame_id = linia2.header.frame_id = linia3.header.frame_id = linia4.header.frame_id = "my_frame";
    shuriken.header.stamp = kolumna.header.stamp = okrag.header.stamp = linia1.header.stamp = linia2.header.stamp = linia3.header.stamp = linia4.header.stamp = ros::Time::now();

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    shuriken.ns = "shuriken"; kolumna.ns = "kolumna"; okrag.ns = "okrag"; linia1.ns = "linia1"; linia2.ns = "linia2"; linia3.ns = "linia3"; linia4.ns = "linia4";
    shuriken.id = 0; kolumna.id = 1; okrag.id = 2; linia1.id = 3; linia2.id = 4; linia3.id = 5; linia4.id = 6;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    shuriken.type = visualization_msgs::Marker::LINE_STRIP;
    kolumna.type = visualization_msgs::Marker::CUBE;
    okrag.type = visualization_msgs::Marker::LINE_STRIP;
    linia1.type = linia2.type = linia3.type = linia4.type = visualization_msgs::Marker::LINE_STRIP;

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    shuriken.action = kolumna.action = okrag.action = linia1.action = linia2.action = linia3.action = linia4.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    kolumna.pose.position.x = 0;
    kolumna.pose.position.y = 0;
    kolumna.pose.position.z = (9.0/2.0);
    kolumna.pose.orientation.x = 0.0;
    kolumna.pose.orientation.y = 0.0;
    kolumna.pose.orientation.z = 0.0;
    kolumna.pose.orientation.w = 1.0;

    geometry_msgs::Point p;

    p.x = -3; p.y = (9.0/2.0); p.z = 0.0+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = -(3.0/4.0); p.y = (9.0/2.0); p.z = -(3.0/4.0)+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = 0; p.y = (9.0/2.0); p.z = -3.0+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = (3.0/4.0); p.y = (9.0/2.0); p.z = -(3.0/4.0)+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = 3; p.y = (9.0/2.0); p.z = 0.0+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = (3.0/4.0); p.y = (9.0/2.0); p.z = (3.0/4.0)+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = 0; p.y = (9.0/2.0); p.z = 3.0+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = -(3.0/4.0); p.y = (9.0/2.0); p.z = (3.0/4.0)+(9.0/2.0);
    shuriken.points.push_back(p);
    p.x = -3.0; p.y = (9.0/2.0); p.z = 0.0+(9.0/2.0);
    shuriken.points.push_back(p);

    // okrag srodek : x = 6, y = 4.5, z = 4.5
    for (uint32_t i = 0; i <= 2*315; i++)
    {
      float x = 2.0*sin(i/100.0f);
      float z = (9.0/2.0) + 2.0*cos(i/100.0f);

      p.x = x; p.y = (9.0/2.0); p.z = z;
      okrag.points.push_back(p);
    }

    p.x = -5.8; p.y = (9.0/2.0); p.z = (9.0/2.0);
    linia1.points.push_back(p);
    p.x = -3.5; p.y = (9.0/2.0); p.z = (9.0/2.0);
    linia1.points.push_back(p);

    p.x = 3.5; p.y = (9.0/2.0); p.z = (9.0/2.0);
    linia2.points.push_back(p);
    p.x = 5.8; p.y = (9.0/2.0); p.z = (9.0/2.0);
    linia2.points.push_back(p);

    p.x = 0; p.y = (9.0/2.0); p.z = 8.8;
    linia3.points.push_back(p);
    p.x = 0; p.y = (9.0/2.0); p.z = 8;
    linia3.points.push_back(p);

    p.x = 0; p.y = (9.0/2.0); p.z = 0.2;
    linia4.points.push_back(p);
    p.x = 0; p.y = (9.0/2.0); p.z = 1;
    linia4.points.push_back(p);

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    kolumna.scale.x = 11.9f; kolumna.scale.y = 8.9f; kolumna.scale.z = 8.9f;

    shuriken.scale.x = okrag.scale.x = linia1.scale.x = linia2.scale.x = linia3.scale.x = linia4.scale.x = 0.1;

    // Set the color -- be sure to set alpha to something non-zero!
    kolumna.color.r = 1/100; kolumna.color.g = 31/100; kolumna.color.b = 100/100; kolumna.color.a = 1.0f;
    shuriken.color.r = okrag.color.r = linia1.color.r = linia2.color.r = linia3.color.r = linia4.color.r = 1.0f;
    shuriken.color.g = okrag.color.g = linia1.color.g = linia2.color.g = linia3.color.g = linia4.color.g = 1.0f;
    shuriken.color.b = okrag.color.b = linia1.color.b = linia2.color.b = linia3.color.b = linia4.color.b = 1.0f;
    shuriken.color.a = okrag.color.a = linia1.color.a = linia2.color.a = linia3.color.a = linia4.color.a = 1.0f;

    shuriken.lifetime = kolumna.lifetime = okrag.lifetime = linia1.lifetime = linia2.lifetime = linia3.lifetime = linia4.lifetime = ros::Duration();

    // Publish the marker
    marker_pub.publish(shuriken); marker_pub.publish(kolumna); marker_pub.publish(okrag); marker_pub.publish(linia1); marker_pub.publish(linia2); marker_pub.publish(linia3); marker_pub.publish(linia4);

    r.sleep();
  }
}
